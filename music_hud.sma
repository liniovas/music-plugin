#include < amxmodx >
#include < music_library >

#pragma semicolon 1

/* Macros */
#define _set_player_bit(%1,%2)   ( %1 |=  ( 1 << ( %2 & 31 ) ) )
#define _get_player_bit(%1,%2)   ( %1 &   ( 1 << ( %2 & 31 ) ) )
#define _clear_player_bit(%1,%2) ( %1 &= ~( 1 << ( %2 & 31 ) ) )

/* Definite constants */
#define TASK_HUD 6001

/* Variables */
new _msg_sync, _total_songs = 0;

public plugin_init( ) {
        register_plugin( "Music HUD", "1.0.1", "TBagT" );
        _total_songs = _get_total_song_count( );
	_msg_sync = CreateHudSyncObj( );
}

public client_putinserver( id ) {
        if( !is_user_bot( id ) )
                set_task( 0.3, "_showhud", id + TASK_HUD, _, _, "b" );
}

public client_disconnected( id ) {
        remove_task( id + TASK_HUD );
}

public _showhud( id ) {
        id -= TASK_HUD;

        if( !_can_listen( id, true ) || !_is_listening( id ) )
                return;

        new _hud_information[ 256 ], _song[ 64 ];
        _get_current_song_name( id, _song, charsmax( _song ) );

        if( _is_in_category( id ) ) {
                new _category[ 32 ];
                _get_current_cat_name( id, _category, charsmax( _category ) );
                formatex( _hud_information, charsmax( _hud_information ), "%L %s | %d / %d^n%L %s ^n%L %d%", LANG_PLAYER, "SONG_CATEGORY", _category, _get_current_index_in_cat( id ), _get_category_song_count( id ), LANG_PLAYER, "SONG_NAME", _song, LANG_PLAYER, "CURRENT_VOLUME", floatround( _get_volume( id ) * 100 ) );
        } else {
                formatex( _hud_information, charsmax( _hud_information ), "%L %d %L %d | %s ^n%L %d%", LANG_PLAYER, "SONG_NAME", _get_current_song_index( id ), LANG_PLAYER, "OUT_OF", _total_songs, _song, LANG_PLAYER, "CURRENT_VOLUME", floatround( _get_volume( id ) * 100 ) );
        }

        set_hudmessage( 255, 255, 255, 0.02, 0.33, 0, 0.0, 0.6, 0.0, 0.0 );
        ShowSyncHudMsg( id, _msg_sync, "%s", _hud_information );
}
