#include < amxmodx >
#include < amxmisc >
#include < fakemeta >
#include < hamsandwich >

#pragma semicolon 1

/* Macros */
#define _set_player_bit(%1,%2)   ( %1 |=  ( 1 << ( %2 & 31 ) ) )
#define _get_player_bit(%1,%2)   ( %1 &   ( 1 << ( %2 & 31 ) ) )
#define _clear_player_bit(%1,%2) ( %1 &= ~( 1 << ( %2 & 31 ) ) )
#define _valid_player(%1)        ( 1 <= %1 <= _max_players && _get_player_bit( _player_is_connected, %1 ) )

/* Definite Constants */
#define SONG_LENGTH 64
#define SONG_PATH   128
#define SONG_TASK   5955
#define SONG_AD     6000

/* Constants */
const OFFSET_CSMENUCODE = 205;

/* Structures */
enum _category {
        _name_cat[ SONG_LENGTH ],
        Array: _songs,
        _songs_count
};

enum _song {
        _name[ SONG_LENGTH ],
        _path[ SONG_PATH   ],
        _duration
};

/* Variables */
new Array: _categories, _cat_count  = 0;
new Array: _songs_list, _song_count = 0;
new _say_text, _max_players;

/* Player Variables */
new _player_is_alive, _player_is_connected, _player_play_songs,
    _player_current_song[ 33 ], _player_current_cat[ 33 ], _player_listening,
    _player_list_page[ 33 ] = 0, Float: _player_volume[ 33 ];

/* Configuration variables */
new _cvar_play_just_connected, _cvar_show_ad, _cvar_ad_timer,
    _cvar_play_for_dead, _cvar_play_round_end;

/* Cached configuration variables */
new bool:  _cache_play_just_connected = false,
    bool:  _cache_play_round_end = false,
    bool:  _cache_play_for_dead = false,
    bool:  _cache_show_ad  = false,
    Float: _cache_ad_timer = 60.0;

/* Plugin initialization */
public plugin_init( ) {
        // Register plugin
        register_plugin( "Music Player", "2.1.1", "TBagT" );
        register_cvar(   "music_plugin", "2.1.1", FCVAR_SPONLY | FCVAR_SERVER );
        set_cvar_string( "music_plugin", "2.1.1" );

        // Configurations
        _cvar_play_just_connected = register_cvar( "music_for_new_players",    "1" );
        _cvar_play_for_dead       = register_cvar( "music_for_dead_players",   "1" );
        _cvar_play_round_end      = register_cvar( "music_stop_at_round_end",  "1" );
        _cvar_show_ad             = register_cvar( "music_show_ad",            "1" );
        _cvar_ad_timer            = register_cvar( "music_ad_timer",        "60.0" );

        // Pre-round start hook
        register_event( "HLTV", "_hook_pre_start", "a", "1=0", "2=0" );

        // Logevent hooks
        register_logevent( "_hook_round_start", 2, "1=Round_Start" );
        register_logevent( "_hook_round_end",   2, "1=Round_End"   );

        // Player spawn hook
        RegisterHam( Ham_Spawn,  "player", "_hook_player_spawn",  1 );
        RegisterHam( Ham_Killed, "player", "_hook_player_killed", 1 );

        // Register menu command hooks
        register_clcmd( "say /music",      "_hook_music_menu" );
        register_clcmd( "say_team /music", "_hook_music_menu" );

        // Hook for next song
        register_clcmd( "say /next",      "_hook_next_song" );
        register_clcmd( "say_team /next", "_hook_next_song" );

        // Hook for previous song
        register_clcmd( "say /prev",      "_hook_prev_song" );
        register_clcmd( "say_team /prev", "_hook_prev_song" );

        // Hook for random song
        register_clcmd( "say /rand",      "_hook_random_song" );
        register_clcmd( "say_team /rand", "_hook_random_song" );

        // Hook for stopping the current song
        register_clcmd( "say /stop",      "_hook_stop_song" );
        register_clcmd( "say_team /stop", "_hook_stop_song" );

        // Get server information
        _max_players = get_maxplayers( );
        _say_text    = get_user_msgid( "SayText" );

        // Cache configurations
        _hook_pre_start( );

        // Register multi-lang
        register_dictionary( "music_plugin.txt" );
}

/* Plugins required files and etc... */
public plugin_precache( ) {
        _categories = ArrayCreate( _category );
        _songs_list = ArrayCreate( _song );

        // Load cfg with music information
        _load_music( );

        // Check if any music was loaded
        if( !_song_count )
                set_fail_state( "[ MUSIC ]: No songs were registered. [ CHECK HLDS FOR ERROR LOGS ]" );
}

/* Plugin end */
public plugin_end( ) {
        if( _cat_count ) {
                new _cache_category[ _category ];
                for( new _index = 0; _index < _cat_count; _index++ ) {
                        ArrayGetArray( _categories, _index, _cache_category );
                        ArrayDestroy( _cache_category[ _songs ] );
                }
        }

        ArrayDestroy( _songs_list );
        ArrayDestroy( _categories );
}

/* Plugin native functions */
public plugin_natives( ) {
        register_library( "music_library" );

        // Category natives
        register_native( "_get_current_cat_name",     "native_get_category_name" );
        register_native( "_get_current_index_in_cat", "native_get_category_song_index" );
        register_native( "_get_category_song_count",  "native_get_category_song_count" );

        // Current song natives
        register_native( "_get_current_song_name",  "native_get_song_name" );
        register_native( "_get_current_song_index", "native_get_song_index" );
        register_native( "_get_total_song_count",   "native_get_song_count" );

        // Other natives
        register_native( "_can_listen",     "native_can_listen" );
        register_native( "_is_listening",   "native_is_listening" );
        register_native( "_is_in_category", "native_is_in_category" );
        register_native( "_stop_music",     "native_stop_music" );
        register_native( "_get_volume",     "native_get_volume" );
}

/** PLUGIN LOGIC START **/
public _hook_pre_start( ) {
        _cache_play_just_connected = bool:  get_pcvar_num( _cvar_play_just_connected );
        _cache_play_for_dead       = bool:  get_pcvar_num( _cvar_play_for_dead );
        _cache_play_round_end      = !( bool: get_pcvar_num( _cvar_play_round_end ) );
        _cache_show_ad             = bool:  get_pcvar_num( _cvar_show_ad );
        _cache_ad_timer            = Float: get_pcvar_float( _cvar_ad_timer );
}

// Player connection
public client_putinserver( id ) {
        if( is_user_bot( id ) )
                return;

        _set_player_bit( _player_is_connected, id );
        _player_current_song[ id ] = 0;
        _player_current_cat[ id ]  = -1;

        if( _cache_play_just_connected )
                _set_player_bit( _player_play_songs, id );
        else _clear_player_bit( _player_play_songs, id );

        query_client_cvar( id, "MP3Volume", "_get_player_volume" );
}

// Player information change
public client_infochanged( id ) {
        if( _get_player_bit( _player_is_connected, id ) )
                query_client_cvar( id, "MP3Volume", "_get_player_volume" );
}

// Player disconnects
public client_disconnected( id ) {
        if( is_user_bot( id ) )
                return;

        _clear_player_bit( _player_is_connected, id );
        _clear_player_bit( _player_is_alive, id );
        _clear_player_bit( _player_play_songs, id );

        _player_current_song[ id ] = 0;
        _player_current_cat[ id ]  = -1;
        _player_list_page[ id ]    = 0;

        remove_task( SONG_TASK + id );
}

// Player spawns
public _hook_player_spawn( id ) {
        ( !is_user_alive( id ) ) ?
        _clear_player_bit( _player_is_alive, id ) :
        _set_player_bit( _player_is_alive, id );

        // Player music function
        if( _can_play( id, false ) )
                _action_play_song( id, _random_song( id ), false );
}

// Player killed
public _hook_player_killed( id ) {
        if( !_cache_play_for_dead )
                _action_stop_song( id, false );
}

// Round start
public _hook_round_start( ) {
        // Show ad every X seconds
        if( _cache_show_ad )
                set_task( _cache_ad_timer, "_action_show_ad", SONG_AD, _, _, "b" );

        // Play songs for all available players
        _action_play_song_all( );
}

// Show ad
public _action_show_ad( ) {
        client_printcolor( 0, "%L", LANG_PLAYER, "MUSIC_AD" );
}

// Round end
public _hook_round_end( ) {
        // Remove ad
        remove_task( SONG_AD );

        // Stop music for all players
        if( !_cache_play_round_end )
                _action_stop_song_all( );
}

/*==================================================
               [ Music functions ]
==================================================*/
_action_play_song_all( ) {
        // Starts playing music for all players
        new _players[ 32 ], _pnum, _player;
        get_players( _players, _pnum );

        // Loop through every player
        for( new _index = 0; _index < _pnum; _index++ ) {
                _player = _players[ _index ];

                // Check if player wants songs to be played
                if( !_can_play( _player, false ) )
                        continue;

                // Emit play action
                _action_play_song( _player, _random_song( _player ), false );
        }
}

_action_stop_song_all( ) {
        new _players[ 32 ], _pnum;
        get_players( _players, _pnum );

        for( new _index = 0; _index < _pnum; _index++ )
                _action_stop_song( _players[ _index ], false );
}

_action_play_song( id, _song_id, bool: _overplay ) {
        // Check if player can listen to music
        // And if overplay is set
        if( !_can_play( id, _overplay ) )
                return;

        // Just in case stop the music
        _action_stop_song( id, false );

        // Get song data
        new _cache_song[ _song ];
        ArrayGetArray( _songs_list, _song_id, _cache_song );

        // Player state change
        if( _overplay )
                _set_player_bit( _player_play_songs, id );

        // Set player as listening to music
        _set_player_bit( _player_listening, id );
        _player_current_song[ id ] = _song_id;

        // Play music
        client_cmd( id, "mp3 play ^"%s^"", _cache_song[ _path ] );
        set_task( float( _cache_song[ _duration ] ), "_action_song_finish", id + SONG_TASK );
}

public _action_song_finish( id ) {
        // Get players real id
        id -= SONG_TASK;

        // Set player as not listening
        _clear_player_bit( _player_listening, id );

        // Play random song
        _action_play_song( id, _random_song( id ), false );
}

public _action_stop_song( id, bool: _all ) {
        if( _all ) {
                _player_current_cat[ id ] = -1;
                _player_current_song[ id ] = 0;
                _clear_player_bit( _player_play_songs, id );
        }

        _clear_player_bit( _player_listening, id );

        remove_task( id + SONG_TASK );
        client_cmd( id, "mp3 stop" );
}

/*==================================================
               [ MUSIC LIST MENU ]
==================================================*/
public _hook_music_menu( id ) {
        if( !_can_play( id, true ) )
                return PLUGIN_HANDLED;

        new _music_menu_id;
        static _music_menu[ SONG_PATH ];

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MAIN_MENU_TITLE" );
        _music_menu_id = menu_create( _music_menu, "_menu_music_main" );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MUSIC_LIST", _song_count );
        menu_additem( _music_menu_id, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L %L", LANG_PLAYER, "MUSIC_STOP", LANG_PLAYER, ( _get_player_bit( _player_play_songs, id ) ) ? "MUSIC_STOP_INACTIVE" : "MUSIC_STOP_ACTIVE" );
        menu_additem( _music_menu_id, _music_menu );

        // Adds a space
        menu_addblank( _music_menu_id, 0 );

        // Increase volume
        formatex( _music_menu, charsmax( _music_menu ), "%s%L", ( _player_volume[ id ] >= 1.0 ) ? "\d" : "\w", LANG_PLAYER, "INCREASE_VOLUME" );
        menu_additem( _music_menu_id, _music_menu );

        // Decrease volume
        formatex( _music_menu, charsmax( _music_menu ), "%s%L", ( _player_volume[ id ] <= 0.0 ) ? "\d" : "\w", LANG_PLAYER, "DECREASE_VOLUME" );
        menu_additem( _music_menu_id, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_PREVIOUS" );
        menu_setprop( _music_menu_id, MPROP_BACKNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_NEXT" );
        menu_setprop( _music_menu_id, MPROP_NEXTNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_EXIT" );
        menu_setprop( _music_menu_id, MPROP_EXITNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_EXIT" );
        menu_setprop( _music_menu_id, MPROP_EXITNAME, _music_menu );

        set_pdata_int( id, OFFSET_CSMENUCODE, 0 );
        menu_display( id, _music_menu_id, 0 );

        return PLUGIN_HANDLED;
}
        public _menu_music_main( id, _menu, _item ) {
                if( _item == MENU_EXIT || !_can_play( id, true ) ) {
                        menu_destroy( _menu );
                        return PLUGIN_HANDLED;
                }

                switch( _item ) {
                        case 0: _music_menu_full( id );
                        case 1: {
                                _action_stop_song( id, true );
                                _hook_music_menu( id );
                        }
                        case 2: {
                                _player_volume[ id ] = floatclamp( ( _player_volume[ id ] += 0.1 ), 0.0, 1.0 );
                                client_cmd( id, "MP3Volume %.2f", _player_volume[ id ] );
                                _hook_music_menu( id );
                        }
                        case 3: {
                                _player_volume[ id ] = floatclamp( ( _player_volume[ id ] -= 0.1 ), 0.0, 1.0 );
                                client_cmd( id, "MP3Volume %.2f", _player_volume[ id ] );
                                _hook_music_menu( id );
                        }
                }

                menu_destroy( _menu );
                return PLUGIN_HANDLED;
        }

public _music_menu_full( id ) {
        if( !_can_play( id, true ) )
                return PLUGIN_HANDLED;

        new _music_menu_id;
        static _music_menu[ SONG_PATH ];

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MAIN_MENU_TITLE" );
        _music_menu_id = menu_create( _music_menu, "_menu_music_full" );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MAIN_MENU_RANDOM" );
        menu_additem( _music_menu_id, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MUSIC_LIST", _song_count );
        menu_additem( _music_menu_id, _music_menu );

        // Display if there are any categories
        if( _cat_count ) {
                formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MUSIC_CATEGORIES" );
                menu_additem( _music_menu_id, _music_menu );
        }

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_PREVIOUS" );
        menu_setprop( _music_menu_id, MPROP_BACKNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_NEXT" );
        menu_setprop( _music_menu_id, MPROP_NEXTNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_EXIT" );
        menu_setprop( _music_menu_id, MPROP_EXITNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_EXIT" );
        menu_setprop( _music_menu_id, MPROP_EXITNAME, _music_menu );

        set_pdata_int( id, OFFSET_CSMENUCODE, 0 );
        menu_display( id, _music_menu_id, 0 );

        return PLUGIN_HANDLED;
}

public _menu_music_full( id, _menu, _item ) {
        if( _item == MENU_EXIT || !_can_play( id, true ) ) {
                menu_destroy( _menu );
                return PLUGIN_HANDLED;
        }

        switch( _item ) {
                case 0: {
                        _player_current_cat[ id ] = -1;
                        _action_play_song( id, _random_song( id ), true );
                        _hook_music_menu( id );
                }
                case 1: _music_list_menu( id );
                case 2: {
                        if( _cat_count )
                                _category_list_menu( id );
                }
        }

        menu_destroy( _menu );
        return PLUGIN_HANDLED;
}

public _music_list_menu( id ) {
        if( !_can_play( id, true ) )
                return PLUGIN_HANDLED;

        new _music_menu_id, _item_data[ 2 ];
        static _music_menu[ SONG_PATH ], _song_item[ _song ];

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "SONG_LIST_TITLE" );
        _music_menu_id = menu_create( _music_menu, "_menu_music_list" );

        for( new _index = 0; _index < _song_count; _index++ ) {
                ArrayGetArray( _songs_list, _index, _song_item );
                _item_data[ 0 ] = _index;
                _item_data[ 1 ] = 0;

                formatex( _music_menu, charsmax( _music_menu ), "%s%s \r[ \y%d:%02d \r]", ( _player_current_song[ id ] == _index && _get_player_bit( _player_listening, id ) ) ? "\d" : "\w", _song_item[ _name ], _song_item[ _duration ] / 60, _song_item[ _duration ] % 60 );
                menu_additem( _music_menu_id, _music_menu, _item_data );
        }

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_PREVIOUS" );
	menu_setprop( _music_menu_id, MPROP_BACKNAME, _music_menu );

	formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_NEXT" );
	menu_setprop( _music_menu_id, MPROP_NEXTNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_EXIT" );
        menu_setprop( _music_menu_id, MPROP_EXITNAME, _music_menu );

        _player_list_page[ id ] = min( _player_list_page[ id ], menu_pages( _music_menu_id ) - 1 );

        set_pdata_int( id, OFFSET_CSMENUCODE, 0 );
        menu_display( id, _music_menu_id, _player_list_page[ id ] );

        return PLUGIN_HANDLED;
}
        public _menu_music_list( id, _menu, _item ) {
                if( _item == MENU_EXIT || !_can_play( id, true ) ) {
                        _player_list_page[ id ] = 0;
                        menu_destroy( _menu );
                        return PLUGIN_HANDLED;
                }

                new _item_data[ 2 ], _dummy, _index;
                menu_item_getinfo( _menu, _item, _dummy, _item_data, charsmax( _item_data ), _, _, _dummy );

                // Song id
                _index = _item_data[ 0 ];

                // Set as non category
                _player_current_cat[ id ] = -1;

                // Remember the page
                _player_list_page[ id ] = _index / 7;

                // Play the song
                _action_play_song( id, _index, true );

                // Reopen the music list menu
                _music_list_menu( id );

                return PLUGIN_HANDLED;
        }

public _category_list_menu( id ) {
        if( !_can_play( id, true ) || !_cat_count )
                return PLUGIN_HANDLED;

        new _music_menu_id, _item_data[ 2 ];
        static _music_menu[ SONG_PATH ], _cat_item[ _category ];

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "CATEGORY_TITLE" );
        _music_menu_id = menu_create( _music_menu, "_menu_category_list" );

        for( new _index = 0; _index < _cat_count; _index++ ) {
                ArrayGetArray( _categories, _index, _cat_item );
                _item_data[ 0 ] = _index;
                _item_data[ 1 ] = 0;

                formatex( _music_menu, charsmax( _music_menu ), "\w%s \r[ \w%d \r]", _cat_item[ _name_cat ], _cat_item[ _songs_count ] );
                menu_additem( _music_menu_id, _music_menu, _item_data );
        }

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_PREVIOUS" );
	menu_setprop( _music_menu_id, MPROP_BACKNAME, _music_menu );

	formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_NEXT" );
	menu_setprop( _music_menu_id, MPROP_NEXTNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_EXIT" );
        menu_setprop( _music_menu_id, MPROP_EXITNAME, _music_menu );

        set_pdata_int( id, OFFSET_CSMENUCODE, 0 );
        menu_display( id, _music_menu_id, 0 );

        return PLUGIN_HANDLED;
}
        public _menu_category_list( id, _menu, _item ) {
                if( _item == MENU_EXIT || !_can_play( id, true ) || !_cat_count ) {
                        menu_destroy( _menu );
                        return PLUGIN_HANDLED;
                }

                new _item_data[ 2 ], _dummy, _index;
                menu_item_getinfo( _menu, _item, _dummy, _item_data, charsmax( _item_data ), _, _, _dummy );

                // Category id
                _index = _item_data[ 0 ];

                // Open the category
                _menu_category( id, _index );

                return PLUGIN_HANDLED;
        }

public _menu_category( id, _category_id ) {
        if( !_can_play( id, true ) || !_cat_count )
                return PLUGIN_HANDLED;

        new _music_menu_id, _item_data[ 3 ], _cache_id;
        static _music_menu[ SONG_PATH ], _cat_item[ _category ], _song_item[ _song ];

        ArrayGetArray( _categories, _category_id, _cat_item );

        formatex( _music_menu, charsmax( _music_menu ), "%s", _cat_item[ _name_cat ] );
        _music_menu_id = menu_create( _music_menu, "_menu_category_song" );

        for( new _index = 0; _index < _cat_item[ _songs_count ]; _index++ ) {
                _cache_id = ArrayGetCell( _cat_item[ _songs ], _index );
                ArrayGetArray( _songs_list, _cache_id, _song_item );

                _item_data[ 0 ] = _cache_id;
                _item_data[ 1 ] = _category_id;
                _item_data[ 2 ] = 0;

                formatex( _music_menu, charsmax( _music_menu ), "%s%s \r[ \y%d:%02d \r]", ( _player_current_song[ id ] == _cache_id && _player_current_cat[ id ] == _category_id ) ? "\d" : "\w", _song_item[ _name ], _song_item[ _duration ] / 60, _song_item[ _duration ] % 60 );
                menu_additem( _music_menu_id, _music_menu, _item_data );
        }

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_PREVIOUS" );
	menu_setprop( _music_menu_id, MPROP_BACKNAME, _music_menu );

	formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_NEXT" );
	menu_setprop( _music_menu_id, MPROP_NEXTNAME, _music_menu );

        formatex( _music_menu, charsmax( _music_menu ), "%L", LANG_PLAYER, "MENU_EXIT" );
        menu_setprop( _music_menu_id, MPROP_EXITNAME, _music_menu );

        set_pdata_int( id, OFFSET_CSMENUCODE, 0 );
        menu_display( id, _music_menu_id, 0 );

        return PLUGIN_HANDLED;
}
        public _menu_category_song( id, _menu, _item ) {
                if( _item == MENU_EXIT || !_can_play( id, true ) ) {
                        menu_destroy( _menu );
                        return PLUGIN_HANDLED;
                }

                new _item_data[ 3 ], _dummy, _index;
                menu_item_getinfo( _menu, _item, _dummy, _item_data, charsmax( _item_data ), _, _, _dummy );

                // Song id
                _index = _item_data[ 0 ];

                // Set as non category
                _player_current_cat[ id ] = _item_data[ 1 ];

                // Play the song
                _action_play_song( id, _index, true );

                // Reopen the category songs list menu
                _menu_category( id, _item_data[ 1 ] );

                return PLUGIN_HANDLED;
        }

/*==================================================
               [ HELPER FUNCTIONS ]
==================================================*/
public _get_player_volume( id, const cvar[ ], const value[ ] ) {
        _player_volume[ id ] = floatclamp( str_to_float( value ), 0.0, 1.0 );
}

_random_song( id ) {
        new _song_id = -1;
        // Check if player is in a category
        if( _player_current_cat[ id ] > -1 ) {
                // Check how much songs this category has
                new _cache_category[ _category ];
                ArrayGetArray( _categories, _player_current_cat[ id ], _cache_category );

                if( _cache_category[ _songs_count ] > 1 )
                        _song_id = ArrayGetCell( _cache_category[ _songs ], random_num( 0, _cache_category[ _songs_count ] - 1 ) );
        }

        if( _song_id == -1 ) {
                // Set as non category listener
                _player_current_cat[ id ] = -1;
                if( _song_count > 1 ) {
                        do {
                                _song_id = random_num( 0, _song_count -1 );
                        } while( _player_current_song[ id ] == _song_id );
                } else _song_id = random_num( 0, _song_count - 1 );
        }

        return _song_id;
}

_previous_song( id ) {
        if( !_can_play( id, true ) )
                return;

        new _song_id = -1;
        // Check if player is in a category
        if( _player_current_cat[ id ] > -1 ) {
                // Check how much songs this category has
                new _cache_category[ _category ], _song_index;
                ArrayGetArray( _categories, _player_current_cat[ id ], _cache_category );

                if( _cache_category[ _songs_count ] > 1 ) {
                        // Get index in category array
                        _song_index = _get_song_in_category_index( _player_current_song[ id ], _player_current_cat[ id ] );

                        if( 0 == _song_index )
                                _song_id = ArrayGetCell( _cache_category[ _songs ], _cache_category[ _songs_count ] - 1 );
                        else if( -1 != _song_index )
                                _song_id = ArrayGetCell( _cache_category[ _songs ], _song_index - 1 );
                } else _song_id = _player_current_song[ id ];
        }

        if( _song_id == -1 ) {
                // Set as non category listener
                _player_current_cat[ id ] = -1;
                if( _player_current_song[ id ] == 0 )
                        _song_id = _song_count - 1;
                else _song_id = _player_current_song[ id ] - 1;
        }

        _action_play_song( id, _song_id, true );
}

_next_song( id ) {
        if( !_can_play( id, true ) )
                return;

        new _song_id = -1;
        // Check if player is in a category
        if( _player_current_cat[ id ] > -1 ) {
                // Check how much songs this category has
                new _cache_category[ _category ], _song_index;
                ArrayGetArray( _categories, _player_current_cat[ id ], _cache_category );

                if( _cache_category[ _songs_count ] > 1 ) {
                        // Get index in category array
                        _song_index = _get_song_in_category_index( _player_current_song[ id ], _player_current_cat[ id ] );

                        if( _cache_category[ _songs_count ] - 1 == _song_index )
                                _song_id = ArrayGetCell( _cache_category[ _songs ], 0 );
                        else if( -1 != _song_index )
                                _song_id = ArrayGetCell( _cache_category[ _songs ], _song_index + 1 );
                } else _song_id = _player_current_song[ id ];
        }

        if( _song_id == -1 ) {
                // Set as non category listener
                _player_current_cat[ id ] = -1;
                if( _player_current_song[ id ] == _song_count - 1 )
                        _song_id = 0;
                else _song_id = _player_current_song[ id ] + 1;
        }

        _action_play_song( id, _song_id, true );
}

_rand_song( id ) {
        if( !_can_play( id, true ) )
                return;

        _player_current_cat[ id ] = -1;
        _action_play_song( id, _random_song( id ), true );
}

_can_play( id, bool: _overplay ) {
        // Can we play music for selected player ?
        if( !_get_player_bit( _player_is_connected, id )
            || _get_player_bit( _player_listening, id ) && !_overplay
            || !_get_player_bit( _player_is_alive, id ) && !_cache_play_for_dead
            || !_get_player_bit( _player_play_songs, id ) && !_overplay
        ) return false;

        return true;
}

_get_song_in_category_index( _song_id, _category_id ) {
        // Checkups
        if( _song_id > _song_count || _category_id > _cat_count )
                return -1;

        new _cache_category[ _category ];
        ArrayGetArray( _categories, _category_id, _cache_category );

        for( new _index = 0; _index < _cache_category[ _songs_count ]; _index ++ ) {
                if( _song_id == ArrayGetCell( _cache_category[ _songs ], _index ) )
                        return _index;
        }
        return -1;
}

_cat_by_name( const _name_provided[ ] ) {
        new _cache_category[ _category ];
        new _cache_name[ SONG_LENGTH ];

        for( new _index = 0; _index < _cat_count; _index ++ ) {
                ArrayGetArray( _categories, _index, _cache_category );
                formatex( _cache_name, charsmax( _cache_name ), "%s", _cache_category[ _name_cat ] );

                if( equal( _name_provided, _cache_name ) )
                        return _index;
        }
        return -1;
}

_load_music( ) {
        new _config[ SONG_PATH ];
        get_configsdir( _config, charsmax( _config ) );
        formatex( _config, charsmax( _config ), "%s/music_plugin.cfg", _config );

        if( !file_exists( _config ) )
                set_fail_state( "[ MUSIC ]: Song list not found." );

        new _file_pointer = fopen( _config, "r" );
        if( _file_pointer ) {
                new _cat_item[ _category ];
                new _song_item[ _song ];
                new _read_data[ SONG_PATH ],
                    _song_name[ SONG_LENGTH ],
                    _song_path[ SONG_PATH ],
                    _song_cat[ SONG_LENGTH ],
                    _song_duration[ 4 ];

                while( fgets( _file_pointer, _read_data, charsmax( _read_data ) ) ) {
                        if( equal( _read_data, "^n" ) || equal( _read_data, "^r^n" ) )
                                continue;

                        parse(
                                _read_data,
                                _song_name, charsmax( _song_name ) - 1,
                                _song_path, charsmax( _song_path ) - 1,
                                _song_cat,  charsmax( _song_cat )  - 1,
                                _song_duration, 3
                        );

                        if( strlen( _song_name ) < 1 ) {
                                log_error( AMX_ERR_GENERAL, "[ MUSIC ]: Invalid song name." );
                                continue;
                        }

                        if( !file_exists( _song_path ) ) {
                                log_error( AMX_ERR_NOTFOUND, "[ MUSIC ]: %s file not found.", _song_name );
                                continue;
                        }

                        if( containi( _song_path, ".mp3" ) == -1 ) {
                                log_error( AMX_ERR_GENERAL, "[ MUSIC ]: %s is not an .mp3 file.", _song_name );
                                continue;
                        }

                        _song_item[ _duration ] = _:str_to_num( _song_duration );
                        if( _song_item[ _duration ] < 1 ) {
                                log_error( AMX_ERR_GENERAL, "[ MUSIC ]: %s duration is invalid.", _song_name );
                                continue;
                        }

                        _song_item[ _name ] = _song_name;
                        _song_item[ _path ] = _song_path;

                        // Precache the song
                        precache_generic( _song_path );

                        ArrayPushArray( _songs_list, _song_item );
                        _song_count++;

                        if( strlen( _song_cat ) ) {
                                // Check if a category with the name exists
                                new _cat_id = _cat_by_name( _song_cat );
                                if( _cat_id != -1 ) {
                                        // If so get it's index
                                        ArrayGetArray( _categories, _cat_id, _cat_item );
                                        _cat_item[ _songs_count ]++;
                                        ArrayPushCell( _cat_item[ _songs ], _song_count - 1 );
                                        ArraySetArray( _categories, _cat_id, _cat_item );
                                } else {
                                        // If no create a new one
                                        _cat_item[ _name_cat ]    = _song_cat;
                                        _cat_item[ _songs ]       = _:ArrayCreate( 1, 1 );
                                        _cat_item[ _songs_count ] = 1;

                                        ArrayPushCell( _cat_item[ _songs ], _song_count - 1 );
                                        ArrayPushArray( _categories, _cat_item );

                                        _cat_count++;
                                }
                        }
                }

                fclose( _file_pointer );
        } else set_fail_state( "[ MUSIC ]: Failed to open song list." );
}

/*==================================================
                 [ Chat Commands ]
==================================================*/
public _hook_next_song( id ) {
        _next_song( id );
        return PLUGIN_HANDLED;
}

public _hook_prev_song( id ) {
        _previous_song( id );
        return PLUGIN_HANDLED;
}

public _hook_random_song( id ) {
        _rand_song( id );
        return PLUGIN_HANDLED;
}

public _hook_stop_song( id ) {
        _action_stop_song( id, true );
        return PLUGIN_HANDLED;
}

/*==================================================
                    [ Natives ]
==================================================*/
public native_get_category_name( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified." );
                return false;
        }

        new _len = get_param( 3 );
        if( !_get_player_bit( _player_listening, _id ) || _player_current_cat[ _id ] < 0 ) {
                set_string( 2, "", _len );
                return true;
        }

        new _cache_category[ _category ];
        ArrayGetArray( _categories, _player_current_cat[ _id ], _cache_category );
        set_string( 2, _cache_category[ _name_cat ], _len );

        return true;
}

public native_get_category_song_index( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified" );
                return false;
        }

        if( !_get_player_bit( _player_listening, _id ) )
                return false;

        return _get_song_in_category_index( _player_current_song[ _id ], _player_current_cat[ _id ] ) + 1;
}

public native_get_category_song_count( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified" );
                return false;
        }

        if( !_get_player_bit( _player_listening, _id ) )
                return false;

        new _cache_category[ _category ];
        ArrayGetArray( _categories, _player_current_cat[ _id ], _cache_category );

        return _cache_category[ _songs_count ];
}

public native_get_song_name( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified." );
                return false;
        }

        new _len = get_param( 3 );
        if( !_get_player_bit( _player_listening, _id ) ) {
                set_string( 2, "", _len );
                return true;
        }

        new _cache_song[ _song ];
        ArrayGetArray( _songs_list, _player_current_song[ _id ], _cache_song );
        set_string( 2, _cache_song[ _name ], _len );

        return true;
}

public native_get_song_index( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified." );
                return false;
        }

        if( !_get_player_bit( _player_listening, _id ) )
                return false;

        return _player_current_song[ _id ] + 1;
}

public native_get_song_count( _plugin, _params ) {
        return _song_count;
}

public native_can_listen( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified." );
                return false;
        }

        return _can_play( _id, bool: get_param( 2 ) );
}

public native_is_listening( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified." );
                return false;
        }

        return bool: _get_player_bit( _player_listening, _id );
}

public native_is_in_category( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified." );
                return false;
        }

        return ( _player_current_cat[ _id ] > -1 );
}

public native_stop_music( _plugin, _params ) {
        new _id = get_param( 1 );
        if( _id != 0 ) {
                if( !_valid_player( _id ) ) {
                        log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified for stopping music." );
                        return false;
                }
        }

        if( _id == 0 )
                _action_stop_song_all( );
        else _action_stop_song( _id, false );

        return true;
}

public Float:native_get_volume( _plugin, _params ) {
        new _id = get_param( 1 );
        if( !_valid_player( _id ) ) {
                log_error( AMX_ERR_NATIVE, "[ MUSIC ]: Invalid player id specified for getting volume." );
                return 0.0;
        }

        query_client_cvar( _id, "MP3Volume", "_get_player_volume" );

        return _player_volume[ _id ];
}

/*==================================================
                    [ STOCKS ]
==================================================*/
stock client_printcolor( const id, const input[ ], any:... ) {
        static _msg[ 191 ];
        new _count = 1, _players[ 32 ];
        vformat( _msg, 190, input, 3 );

        replace_all( _msg, 190, "!g", "^4" );
        replace_all( _msg, 190, "!y", "^1" );
        replace_all( _msg, 190, "!t", "^3" );

        if( _valid_player( id ) )
                _players[ 0 ] = id;
        else get_players( _players, _count, "ch" );

        for( new _index = 0; _index < _count; _index++ ) {
                message_begin( MSG_ONE_UNRELIABLE, _say_text, _, _players[ _index ] );
                write_byte( _players[ _index ] );
                write_string( _msg );
                message_end( );
        }
 }
