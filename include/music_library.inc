#if defined _music_library_included
        #endinput
#endif
#define _music_library_included

#if AMXX_VERSION_NUM >= 175
	#pragma reqlib music_library
	#if !defined AMXMODX_NOAUTOLOAD
		#pragma loadlib music_library
	#endif
#else
	#pragma library music_library
#endif

/**
 * Get players current category
 *
 * @param id   - The id of the player.
 * @param name - The buffer to store the string in
 * @param len  - Character size of the output buffer.
 * @return       string
 */
native _get_current_cat_name( id, name[ ], len );

/**
 * Get current song index in category
 *
 * @param id - The id of the player
 * @return     integer | false
 */
native _get_current_index_in_cat( id );

/**
 * Get current category song count
 *
 * @param id - The id of the player
 * @return     integer | false
 */
native _get_category_song_count( id );

/**
 * Get current songs name
 *
 * @param id   - The id of the player
 * @param name - The buffer to store the string in
 * @param len  - Character size of the output buffer.
 * @return       string
 */
native _get_current_song_name( id, name[ ], len );

/**
 * Get current songs index
 *
 * @param id - The id of the player
 * @return     integer | false
 */
native _get_current_song_index( id );

/**
 * Gets the total amount of songs
 *
 * @return integer
 */
native _get_total_song_count( );

/**
 * Check if player can listen to music
 *
 * @param id - The id of the player
 * @param overwrite - Check with user overwritable
 * @return true | false
 */
native _can_listen( id, bool: overwrite = false );

/**
 * Checks if player is listening to something
 *
 * @param id - The id of the player
 * @return     true | false
 */
native _is_listening( id );

/**
 * Checks if player is listening from a category
 *
 * @param id - The id of the player
 * @return     true | false
 */
native _is_in_category( id );

/**
 * Stops music untill next action to play is emitted
 *
 * @param id - 0 or specific player id
 * @return     true on success | false of failure
 */
native _stop_music( id = 0 );

/**
 * Gets players music volume
 *
 * @param id - specific player id.
 * @return     float
 */
 native Float:_get_volume( id );
